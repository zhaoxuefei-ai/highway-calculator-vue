/**
 * 全站路由配置
 *
 * 建议:
 * 1. 代码中路由统一使用name属性跳转(不使用path属性)
 */
import Vue from 'vue'
import Router from 'vue-router'
import http from '@/utils/httpRequest'
import { isURL } from '@/utils/validate'
import { clearLoginInfo } from '@/utils'

Vue.use(Router)

// 开发环境不使用懒加载, 因为懒加载页面太多的话会造成webpack热更新太慢, 所以只有生产环境使用懒加载
const _import = require('./import-' + process.env.NODE_ENV)

// 全局路由(无需嵌套上左右整体布局)
const globalRoutes = [
  { path: '/404', component: _import('common/404'), name: '404', meta: { title: '404未找到' } },
  { path: '/login', component: _import('common/login'), name: 'login', meta: { title: '登录' } },
    { path: '/news-noLogin', component: _import('common/news-noLogin'), name: 'news-noLogin', meta: { title: '新闻详情' } },
    { path: '/four_news-noLogin', component: _import('common/four_news-noLogin'), name: 'four_news-noLogin', meta: { title: '新闻列表' } }
]

// 主入口路由(需嵌套上左右整体布局)
const mainRoutes = {
  path: '/',
  component: _import('main'),
  name: 'main',
  redirect: { name: 'home' },
  meta: { title: '主入口整体布局' },
  children: [
    // 通过meta对象设置路由展示方式
    // 1. isTab: 是否通过tab展示内容, true: 是, false: 否
    // 2. iframeUrl: 是否通过iframe嵌套展示内容, '以http[s]://开头': 是, '': 否
    // 提示: 如需要通过iframe嵌套展示内容, 但不通过tab打开, 请自行创建组件使用iframe处理!
    { path: '/news', component: _import('modules/news/news'), name: 'news', meta: { title: '新闻详情' } },
    { path: '/four_news', component: _import('modules/news/four_news'), name: 'four_news', meta: { title: '新闻列表' } },
    { path: '/home', component: _import('common/home'), name: 'home', meta: { title: '首页' } },
    { path: '/my-record', component: _import('common/my-record'), name: 'my-record', meta: { title: '我的记录' } }
    // { path: '/calculator/jlsfsy', component: _import('modules/calculator/jlsfsy'), name: 'jlsfsy', meta: { title: '集料筛分试验' } }
    // { path: '/demo-echarts', component: _import('demo/echarts'), name: 'demo-echarts', meta: { title: 'demo-echarts', isTab: true } },
    // { path: '/demo-ueditor', component: _import('demo/ueditor'), name: 'demo-ueditor', meta: { title: 'demo-ueditor', isTab: true } }
  ],
  beforeEnter (to, from, next) {
    let token = Vue.cookie.get('token')
    if (!token || !/\S/.test(token)) {
      clearLoginInfo()
      next({ name: 'login' })
    }
    next()
  }
}

const router = new Router({
  mode: 'hash',
  scrollBehavior: () => ({ y: 0 }),
  isAddDynamicCalculatorRoutes: false, // 是否已经添加动态(计算器)路由
  routes: globalRoutes.concat(mainRoutes),

})


router.beforeEach((to, from, next) => {
  // 添加动态(计算器)路由
  // 1. 已经添加 or 全局路由, 直接访问
  // 2. 获取计算器列表, 添加并保存本地存储
  if (router.options.isAddDynamicCalculatorRoutes || fnCurrentRouteType(to, globalRoutes) === 'global') {
    next()
  } else {
    http({
      url: http.adornUrl('/app/cal/nav'),
      method: 'get',
      params: http.adornParams()
    }).then(({data}) => {
      if (data && data.code === 0) {
        fnAddDynamicCalculatorRoutes(data.calculatorList)
        router.options.isAddDynamicCalculatorRoutes = true
        sessionStorage.setItem('calculatorList', JSON.stringify(data.calculatorList || '[]'))
        next({ ...to, replace: true })
      } else {
        sessionStorage.setItem('calculatorList', '[]')
        next()
      }
    }).catch((e) => {
        // 想不出来登陆后还需要什么权限
      console.log(`%c${e} 请求计算器列表失败，跳转至登录页！！`, 'color:blue')
      router.push({ name: 'login' })
    })
  }
})

/**
 * 判断当前路由类型, global: 全局路由, main: 主入口路由
 * @param {*} route 当前路由
 */
function fnCurrentRouteType (route, globalRoutes = []) {
  var temp = []
  for (var i = 0; i < globalRoutes.length; i++) {
    if (route.path === globalRoutes[i].path) {
      return 'global'
    } else if (globalRoutes[i].children && globalRoutes[i].children.length >= 1) {
      temp = temp.concat(globalRoutes[i].children)
    }
  }
  return temp.length >= 1 ? fnCurrentRouteType(route, temp) : 'main'
}

/**
 * 添加动态(计算器)路由
 * @param {*} calculatorList 计算器列表
 * @param {*} routes 递归创建的动态(计算器)路由
 */
function fnAddDynamicCalculatorRoutes (calculatorList = [], routes = []) {
  var temp = []
  for (var i = 0; i < calculatorList.length; i++) {
    if (calculatorList[i].list && calculatorList[i].list.length >= 1) {
      temp = temp.concat(calculatorList[i].list)
    } else if (calculatorList[i].url && /\S/.test(calculatorList[i].url)) {
      calculatorList[i].url = calculatorList[i].url.replace(/^\//, '')
      var route = {
        path: calculatorList[i].url.replace('/', '-'),
        component: null,
        name: calculatorList[i].url.replace('/', '-'),
        meta: {
          menuId: calculatorList[i].menuId,
          title: calculatorList[i].name,
          isDynamic: true,
          isTab: true,
          iframeUrl: ''
        }
      }
      // url以http[s]://开头, 通过iframe展示
      if (isURL(calculatorList[i].url)) {
        route['path'] = `i-${calculatorList[i].menuId}`
        route['name'] = `i-${calculatorList[i].menuId}`
        route['meta']['iframeUrl'] = calculatorList[i].url
      } else {
        try {
          route['component'] = _import(`modules/cal/${calculatorList[i].url}`) || null
        } catch (e) {}
      }
      routes.push(route)
    }
  }
  if (temp.length >= 1) {
    fnAddDynamicCalculatorRoutes(temp, routes)
  } else {
    mainRoutes.name = 'main-dynamic'
    mainRoutes.children = routes
      console.log("routes==="+routes)
    router.addRoutes([
      mainRoutes,
      { path: '*', redirect: { name: '404' } }
    ])
    sessionStorage.setItem('dynamicCalculatorRoutes', JSON.stringify(mainRoutes.children || '[]'))
    console.log('\n')
    console.log('%c!<-------------------- 动态(计算器)路由 s -------------------->', 'color:blue')
    console.log(mainRoutes.children)
    console.log('%c!<-------------------- 动态(计算器)路由 e -------------------->', 'color:blue')
  }
}

export default router

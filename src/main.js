import Vue from 'vue'
import App from '@/App'
import router from '@/router'                 // api: https://github.com/vuejs/vue-router
import store from '@/store'                   // api: https://github.com/vuejs/vuex
import VueCookie from 'vue-cookie'            // api: https://github.com/alfhen/vue-cookie
import '@/element-ui'                         // api: https://github.com/ElemeFE/element
import '@/icons'                              // api: http://www.iconfont.cn/
import '@/element-ui-theme'
import '@/components'                         // 项目通用组件
import httpRequest from '@/utils/httpRequest' // api: https://github.com/axios/axios
import {isAuth, format, NumFormat} from '@/utils'
import cloneDeep from 'lodash/cloneDeep'

import ECharts from 'vue-echarts' // refers to components/ECharts.vue in webpack
import 'echarts/lib/chart/bar'
import 'echarts/lib/component/markLine'
import 'echarts/lib/component/legend'
// import ECharts modules manually to reduce bundle size

// import Print from 'vue-print-nb'
import Print from '@/utils/print'
import Export2PDF from '@/utils/Export2PDF'

Vue.use(Export2PDF)

Vue.component('v-chart', ECharts)
Vue.use(Print)
Vue.use(VueCookie)
Vue.config.productionTip = false

// 挂载全局
Vue.prototype.$http = httpRequest // ajax请求方法
Vue.prototype.isAuth = isAuth     // 权限方法
Vue.prototype.$DateFormat = format // 日期格式
Vue.prototype.$NumFormat = NumFormat // 日期格式

// 保存整站vuex本地储存初始状态
window.SITE_CONFIG['storeState'] = cloneDeep(store.state)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {App}
})

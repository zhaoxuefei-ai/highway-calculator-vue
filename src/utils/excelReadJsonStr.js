let strPrefix = 'excelData'
let root = 'this.'

/*
let that = this
import('../excelReadJsonStr/utils')
  .then(util => {
    let strList = []
    util.dfs(result, 'result', strList)
    console.log('\n'.join(strList))
  })
*/

/**
 * search excelData from gaven json.
 * @param obj obj to search
 * @param parent obj'name
 */
export function dfs (obj, parent, strList) {
  if (typeof obj === 'string' && (obj.startsWith(strPrefix))) {
    strList.push(`${root}${obj} = ${parent}`)
  } else if (obj instanceof Array) {
    for (let i = 0; i < obj.length; i++) {
      dfs(obj[i], `${parent}[${i}]`, strList)
    }
  } else if (obj instanceof Object) {
    Object.keys(obj).forEach(key => {
      dfs(obj[key], `${parent}['${key}']`, strList)
    })
  }
}

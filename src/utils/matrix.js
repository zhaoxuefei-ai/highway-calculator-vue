// 求行列式
export function determinant (matrix) {
  let order = matrix.length, cofactor, result = 0
  if (order === 1) {
    return matrix[0][0]
  }
  for (let i = 0; i < order; i++) {
    cofactor = []
    for (let j = 0; j < order - 1; j++) {
      cofactor[j] = []
      for (let k = 0; k < order - 1; k++) {
        cofactor[j][k] = matrix[j + 1][k < i ? k : k + 1]
      }
    }
    result += matrix[0][i] * Math.pow(-1, i) * determinant(cofactor)
  }
  return result
}

// 矩阵数乘
export function scalarMultiply (num, matrix) {
  let row = matrix.length,
    col = matrix[0].length,
    result = []
  for (let i = 0; i < row; i++) {
    result[i] = []
    for (let j = 0; j < col; j++) {
      result[i][j] = num * matrix[i][j]
    }
  }
  return result
}

// 矩阵转置
export function transpose (matrix) {
  let row = matrix.length,
    col = matrix[0].length,
    result = []
  for (let i = 0; i < col; i++) {
    result[i] = []
    for (let j = 0; j < row; j++) {
      result[i][j] = matrix[j][i]
    }
  }
  return result
}

// 矢量内积
export function dotProduct (vector1, vector2) {
  let n = Math.min(vector1.length, vector2.length),
    result = 0
  for (let i = 0; i < n; i++) {
    result += vector1[i] * vector2[i]
  }
  return result
}

// 矩阵乘法
export function multiply (matrix1, matrix2) {
  if (matrix1[0].length !== matrix2.length) {
    return false
  }
  let row = matrix1.length,
    col = matrix2[0].length,
    matrix2_t = transpose(matrix2)
  result = []
  for (let i = 0; i < row; i++) {
    result[i] = []
    for (let j = 0; j < col; j++) {
      result[i][j] = dotProduct(matrix1[i], matrix2_t[j])
    }
  }
  return result
}

// 求逆矩阵
export function inverse (matrix) {
  if (determinant(matrix) === 0) {
    return false
  }

    // 求代数余子式
  function cofactor (matrix, row, col) {
    let order = matrix.length,
      new_matrix = [],
      _row, _col
    for (let i = 0; i < order - 1; i++) {
      new_matrix[i] = []
      _row = i < row ? i : i + 1
      for (let j = 0; j < order - 1; j++) {
        _col = j < col ? j : j + 1
        new_matrix[i][j] = matrix[_row][_col]
      }
    }
    return Math.pow(-1, row + col) * determinant(new_matrix)
  }

  let order = matrix.length,
    adjoint = []
  for (let i = 0; i < order; i++) {
    adjoint[i] = []
    for (let j = 0; j < order; j++) {
      adjoint[i][j] = cofactor(matrix, j, i)
    }
  }
  return scalarMultiply(1 / determinant(matrix), adjoint)
}

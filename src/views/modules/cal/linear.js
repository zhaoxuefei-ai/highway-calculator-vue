const tf = require('@tensorflow/tfjs')

// 求行列式
function determinant (matrix) {
    let order = matrix.length, cofactor, result = 0
    if (order === 1) {
        return matrix[0][0]
    }
    for (let i = 0; i < order; i++) {
        cofactor = []
        for (let j = 0; j < order - 1; j++) {
            cofactor[j] = []
            for (let k = 0; k < order - 1; k++) {
                cofactor[j][k] = matrix[j + 1][k < i ? k : k + 1]
            }
        }
        result += matrix[0][i] * Math.pow(-1, i) * determinant(cofactor)
    }
    return result
}

// 矩阵数乘
function scalarMultiply (num, matrix) {
    let row = matrix.length,
        col = matrix[0].length,
        result = []
    for (let i = 0; i < row; i++) {
        result[i] = []
        for (let j = 0; j < col; j++) {
            result[i][j] = num * matrix[i][j]
        }
    }
    return result
}

// 矩阵转置
function transpose (matrix) {
    let row = matrix.length,
        col = matrix[0].length,
        result = []
    for (let i = 0; i < col; i++) {
        result[i] = []
        for (let j = 0; j < row; j++) {
            result[i][j] = matrix[j][i]
        }
    }
    return result
}

// 矢量内积
function dotProduct (vector1, vector2) {
    let n = Math.min(vector1.length, vector2.length),
        result = 0
    for (let i = 0; i < n; i++) {
        result += vector1[i] * vector2[i]
    }
    return result
}

// 矩阵乘法
function multiply (matrix1, matrix2) {
    if (matrix1[0].length !== matrix2.length) {
        return false
    }
    let row = matrix1.length,
        col = matrix2[0].length,
        matrix2_t = transpose(matrix2)
    result = []
    for (let i = 0; i < row; i++) {
        result[i] = []
        for (let j = 0; j < col; j++) {
            result[i][j] = dotProduct(matrix1[i], matrix2_t[j])
        }
    }
    return result
}

// 求逆矩阵
function inverse (matrix) {
    if (determinant(matrix) === 0) {
        return false
    }

    // 求代数余子式
    function cofactor (matrix, row, col) {
        let order = matrix.length,
            new_matrix = [],
            _row, _col
        for (let i = 0; i < order - 1; i++) {
            new_matrix[i] = []
            _row = i < row ? i : i + 1
            for (let j = 0; j < order - 1; j++) {
                _col = j < col ? j : j + 1
                new_matrix[i][j] = matrix[_row][_col]
            }
        }
        return Math.pow(-1, row + col) * determinant(new_matrix)
    }

    let order = matrix.length,
        adjoint = []
    for (let i = 0; i < order; i++) {
        adjoint[i] = []
        for (let j = 0; j < order; j++) {
            adjoint[i][j] = cofactor(matrix, j, i)
        }
    }
    return scalarMultiply(1 / determinant(matrix), adjoint)
}

function LinearRegressionFormulaMethod (x_array, y_array) {
    // 定义一个线性回归模型。 使用公式法
    const x = tf.tensor2d(x_array, [x_array.length, x_array[0].length])
    const y = tf.tensor2d(y_array, [y_array.length, 1])
    // console.log('x ' + x)
    // console.log('y ' + y)
    const x_T = tf.transpose(x)
    // console.log('x_T ' + x_T)
    const x_T_matmul_x = tf.matMul(x_T, x)
    // console.log('x_T_matmul_x ' + x_T_matmul_x)
    let x_T_matmul_x_array = JSON.parse(x_T_matmul_x.toString().slice(6, x_T_matmul_x.toString().length))
    // console.log(x_T_matmul_x_array)
    const x_inverse_array = inverse(x_T_matmul_x_array)
    const x_inverse = tf.tensor2d(x_inverse_array, [x_inverse_array.length, x_inverse_array[0].length])
    // console.log('x_inverse ' + x_inverse)
    const x_inverse_matMal_x_t = tf.matMul(x_inverse, x_T)
    // console.log('x_inverse_matMal_x_t ' + x_inverse_matMal_x_t)
    const target = tf.matMul(x_inverse_matMal_x_t, y)
    // console.log('target ' + target)
    const sum = tf.sum(target)
    // console.log('sum ' + sum)
    const target_array = JSON.parse(tf.transpose(target).toString().slice(13, tf.transpose(target).toString().length - 2))
    const y_real = tf.matMul(x, target)
    const y_real_array = JSON.parse(tf.transpose(y_real).toString().slice(13, tf.transpose(y_real).toString().length - 2))
    return [y_real_array, target_array]
}


var lastData = null

function isInConstraint (w, iter, limit) {
    let data = w.dataSync()
    for (let i = 0; i < data.length; i++) {
        if (data[i] > limit.wMax || data[i] < limit.wMin) {
            return false
        }
        if (iter > 1) {
            let mean = data.map(function (item, index) {
                return item - lastData[index]
            })
            // console.log(mean)
            for (let j = 0; j < mean.length; j++) {
                if ((mean[j] < limit.stepMin && mean[j] > 0) || (mean[j] > 0 - limit.stepMin && mean[j] < 0)) {
                    return false
                }
            }
        }
    }
    return true
}

async function train (xs, ys, ws, numIterations, optimizer, limit) {
    for (let iter = 0; iter < numIterations; iter++) {
        lastData = ws.dataSync()
        optimizer.minimize(() => {
            const pred = tf.tidy(() => {
                return tf.matMul(xs, ws)
            })
            return pred.sub(ys).square().mean()
        })
        if (!isInConstraint(ws, iter, limit)) {
            break
        }
        await tf.nextFrame()
    }
}

async function LinearRegressionGradientDescent (x_array, y_array) {
    //  使用梯度下降 可以完成数值限制
    const x = tf.tensor2d(x_array, [x_array.length, x_array[0].length])
    const y = tf.tensor2d(y_array, [y_array.length, 1])
    let w_array = [...Array(x_array[0].length)].map(_ => 0)
    // console.log(w_array)
    const w = tf.variable(tf.tensor2d(w_array, [x_array[0].length, 1]))
    const numIterations = 10000
    const learningRate = 0.000031
    const limit = {
        wMax: 0.8,
        wMin: 0.02,
        stepMin: 0.00000001
    }
    const optimizer = tf.train.sgd(learningRate)

    await train(x, y, w, numIterations, optimizer, limit)
    const dd = await w.dataSync()
    console.log(dd)
    // const yy = await tf.matMul(x, w).dataSync()
    // return dd
}

let x_array = [
    [100.0, 100.0, 100.0, 100.0, 100.0, 100.0],
    [73.5, 100.0, 100.0, 100.0, 100.0, 100.0],
    [14.1, 90.6, 100.0, 100.0, 100.0, 100.0],
    [4.2, 68.2, 100.0, 100.0, 100.0, 100.0],
    [1.7, 38.2, 100.0, 100.0, 100.0, 100.0],
    [1.0, 8.4, 99.3, 100.0, 100.0, 100.0],
    [0.2, 0.6, 31.0, 98.2, 100.0, 100.0],
    [0.2, 0.2, 6.5, 18.5, 79.8, 100.0],
    [0.2, 0.2, 4.0, 6.8, 51.5, 100.0],
    [0.2, 0.2, 0.8, 4.3, 39.5, 100.0],
    [0.2, 0.2, 0.8, 2.4, 23.3, 99.4],
    [0.2, 0.2, 0.8, 2.0, 17.4, 99.1],
    [0.2, 0.2, 0.8, 0.7, 12.7, 93.5]
]

let y_array = [100, 96, 75, 60, 55, 44, 30, 21, 16, 11, 8, 5, 4]

// let [y_real_array, target_array] = LinearRegressionFormulaMethod(x_array, y_array)
// console.log(y_real_array, target_array)
// let da = await LinearRegressionGradientDescent(x_array, y_array)
// console.log(da)
LinearRegressionGradientDescent(x_array, y_array)

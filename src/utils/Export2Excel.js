require('script-loader!file-saver')
import XLSX from 'xlsx'

// 分割参数
var blankLine = 3

function Workbook() {
    if (!(this instanceof Workbook)) return new Workbook()
    this.SheetNames = []
    this.Sheets = {}
}

function s2ab(s) {
    var buf = new ArrayBuffer(s.length)
    var view = new Uint8Array(buf)
    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF
    return buf
}

// idList: 表格ID 传入(List)
// sheetName: 工作表名 传入(List)
// fileName: 导出文件名(*.xlsx)

export function export_table_to_excel(idList, sheetName, fileName) {

    var workBook = new Workbook()

    var shArr = []
    // 遍历传入的表格列表，并加入workBook
    for (var a = 0; a < idList.length; a++) {
        var theTable = document.getElementById(idList[a])
        //新增表
        var workSheet = XLSX.utils.table_to_sheet(theTable, { raw: true })
        shArr[a] = workSheet
        if (a > 0) {
            margeSheet(shArr[0], shArr[a])
        }
    }
    workBook.SheetNames.push(sheetName[0])
    workBook.Sheets[sheetName[0]] = shArr[0]

    var wbout = XLSX.write(workBook, {
        bookType: 'xlsx',
        bookSST: false,
        type: 'binary'
    })

    saveAs(new Blob([s2ab(wbout)], {
        type: 'application/octet-stream'
    }), fileName)
}

function margeSheet(mainSheet, otherSheet) {
    // !merges: Array(17)
    //0: {e: {r: 0, c: 7},s: {r: 0, c: 0}}
    // !ref: "A1:J29"
    // let sh = {A1:{t:'s',v:'100'},B1:{t:'s',v:'100'},A2:{t:'s',v:'100'},BB12:{t:'s',v:'100'}}
    let mainSheetMaxNum = findMax(mainSheet)
    // handelMerges
    margeMerges(mainSheet['!merges'], otherSheet['!merges'], mainSheetMaxNum + blankLine)
    // handel ref
    mainSheet['!ref'] = margeRef(mainSheet['!ref'], otherSheet['!ref'], mainSheetMaxNum + blankLine)
    // handel data
    margeData(mainSheet, otherSheet,)

}
function margeMerges(mArr1, mArr2, line) {
    // 主不变 从的所有行加上line
    for (let i = 0; i < mArr2.length; i++) {
        const m2 = mArr2[i];
        // 所有行相加
        m2.s.r += line
        m2.e.r += line
        // 合并
        mArr1.push(m2)
    }
}
function margeRef(ref1, ref2, line) {
    let k1 = ref1.split(':')
    let k2 = ref2.split(':')
    let [a11, n11] = findAlphaNum(k1[1])
    let [a21, n21] = findAlphaNum(k2[1])
    // 找到那个比较大
    let maxAlpha = findMaxAlpha(a11, a21)
    return 'A1:' + maxAlpha + (n21 + n11+blankLine)
}
function findMaxAlpha(a1, a2) {
    let n1 = calAlpha(a1)
    let n2 = calAlpha(a2)
    return n1 > n2 ? a1 : a2
}
function calAlpha(a) {
    let sum = 0
    for (let i = 0; i < a.length; i++) {
        const ch = a[i];
        let num = ch.charCodeAt() - 'A'.charCodeAt() + 1
        sum = sum * 26 + num
    }
    return sum
}
function isData(key) {
    return key !== '!merges' && key !== '!ref'
}
function margeData(mSh, oSh) {
    let mainSheetMaxNum = findMax(mSh)
    oSh = enlargeNumInSheet(oSh, mainSheetMaxNum + blankLine)
    objAdd(mSh, oSh)
}

function objAdd(obj1, obj2) {
    let keys2 = Object.keys(obj2)
    keys2 = keys2.filter(el => { return isData(el) })
    for (let i = 0; i < keys2.length; i++) {
        const key2 = keys2[i];
        obj1[key2] = obj2[key2]
    }
}
function enlargeNumInSheet(sheet, num) {
    let map = getMap(sheet, num)
    let copy = {}
    let keys = Object.keys(sheet)
    keys = keys.filter(el => { return isData(el) })
    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        copy[map[key]] = sheet[key]
    }
    return copy
}
function getMap(sheet, size) {
    let keys = Object.keys(sheet)
    keys = keys.filter(el => { return isData(el) })
    let map = {}
    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        let [alpha, num] = findAlphaNum(key)
        map[key] = alpha + (parseInt(num) + size)
    }
    return map
}
function findMax(sheet) {
    let keys = Object.keys(sheet)
    keys = keys.filter(el => { return isData(el) })
    let max = -1
    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        let [alpha, num] = findAlphaNum(key)
        max = num > max ? num : max
    }
    return max
}
function findAlphaNum(key) {
    let numStart = -1;
    for (let j = 0; j < key.length; j++) {
        const ch = key[j];
        if (ch.charCodeAt() < 65) {
            numStart = j
            break
        }
    }
    let num = parseInt(key.slice(numStart))
    let alpha = key.slice(0, numStart)
    return [alpha, num]
}
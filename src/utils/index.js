import Vue from 'vue'
import router from '@/router'
import store from '@/store'

export function getXInYMaxMin (arrayN2) {
    let [xMax,xMain] = [0,0]
    let [yMax,yMain] = [0,0]
    let comp = function(v1,v2){
        if(v1[1]===v2[1]){return 0}
        return v1[1]>v2[1]?1:-1
    }
    // arrayN2.sort(comp)
    return [arrayN2[0][0],arrayN2[arrayN2.length-1][0]]
}

export function readExcelCell (excelData, col, row, type) {
    if (!(excelData instanceof Array)) {
        return null
    }
    // find data
    let cell = excelData[col]['__EMPTY_' + row]
    // float str
    if (type === 'number') {
        return parseFloat(cell)
    } else if (type === 'string') {
        return JSON.stringify(cell)
    } else {
        return cell
    }
}

export function clearObjectData (obj) {
    if (typeof obj === 'number') {
        obj = ''
    } else if (typeof obj === 'string' && !isNaN(parseFloat(obj))) {
        // parseFloat() 有个bug 如果开始为数字 就会转换成数字
        obj = ''
    } else if (obj instanceof Array) {
        // 假设所有Array中的元素都是同一种类
        obj = obj.map(x => clearObjectData(x))
    } else if (obj instanceof Object) {
        Object.keys(obj).forEach(key => {
            obj[key] = clearObjectData(obj[key])
        })
    }
    return obj
}

/**
 * Round a number to a precision, specificed in number of decimal places
 *
 * @param {number} number - The number to round
 * @param {number} precision - The number of decimal places to round to:
 *                             > 0 means decimals, < 0 means powers of 10
 *
 *
 * @return {numbr} - The number, rounded
 */
export function round (number, precision) {
    let factor = Math.pow(10, precision)
    return Math.round(number * factor) / factor
}

export function sum (arr) {
    let s = 0
    for (let i = arr.length - 1; i >= 0; i--) {
        s = add(s, arr[i])
    }
    return s
}

export function add (a, b) {
    return parseFloat(a) + parseFloat(b)
}

export function sub (a, b) {
    return parseFloat(a) - parseFloat(b)
}

export function mul (a, b) {
    return parseFloat(a) * parseFloat(b)
}

export function div (a, b) {
    return parseFloat(a) / parseFloat(b)
}

/**
 * 格式化数字
 * return str
 */
export function NumFormat (fixedCount, numArray) {
    for (let i = 0; i < numArray.length; i++) {
        numArray[i] = NumFormatSingle(fixedCount, numArray[i])
    }
    return numArray
}

function NumFormatSingle (fixedCount, num) {
    // num = parseFloat(num)
    // console.log("num=="+typeof num)
    if (typeof num === 'number') {
        return num.toFixed(fixedCount)
    } else if (typeof num === 'string') {
        return parseFloat(num).toFixed(fixedCount)
    } else {
        return num
    }
}

/**
 * 格式化时间 yyyy-MM-dd HH:mm:ss
 */
export function format (d) {
    let format = new Date(d.getTime() - d.getTimezoneOffset() * 60000).toISOString().replace('T', ' ').replace(/\..+$/, '')
    return format + ''
}

/**
 * 获取uuid
 */
export function getUUID () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
        return (c === 'x' ? (Math.random() * 16 | 0) : ('r&0x3' | '0x8')).toString(16)
    })
}

/**
 * 是否有权限
 * @param {*} key
 */
export function isAuth (key) {
    return JSON.parse(sessionStorage.getItem('permissions') || '[]').indexOf(key) !== -1 || false
}

/**
 * 树形数据转换
 * @param {*} data
 * @param {*} id
 * @param {*} pid
 */
export function treeDataTranslate (data, id = 'id', pid = 'parentId') {
    var res = []
    var temp = {}
    for (var i = 0; i < data.length; i++) {
        temp[data[i][id]] = data[i]
    }
    for (var k = 0; k < data.length; k++) {
        if (temp[data[k][pid]] && data[k][id] !== data[k][pid]) {
            if (!temp[data[k][pid]]['children']) {
                temp[data[k][pid]]['children'] = []
            }
            if (!temp[data[k][pid]]['_level']) {
                temp[data[k][pid]]['_level'] = 1
            }
            data[k]['_level'] = temp[data[k][pid]]._level + 1
            temp[data[k][pid]]['children'].push(data[k])
        } else {
            res.push(data[k])
        }
    }
    return res
}

/**
 * 清除登录信息
 */
export function clearLoginInfo () {
    Vue.cookie.delete('token')
    store.commit('resetStore')
    router.options.isAddDynamicMenuRoutes = false
}

export function rounded(content,digit) {
    if(digit === 0){
        return content;
    }
    let re = /^[0-9]+.?[0-9]*$/;
    if (!re.test(content)) {
        return content;
    }
    if(digit === 1){
        // return Math.round(content * 10) / 10
        return parseFloat(content).toFixed(1)
    }
    if(digit === 2){
        // return Math.round(content * 100) / 100
        return parseFloat(content).toFixed(2)
    }
    if(digit === 3){
        // return Math.round(content * 1000) / 1000
        return parseFloat(content).toFixed(3)
    }
    return 0
}
